<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $data['mainMenu'] = 'dashboard';
        $data['menuName'] = __('Dashboard');


        $data['thisMonthUser'] = User::where(['role' => USER_ROLE])->where('status', '!=', DELETE_STATUS)->whereRaw("created_at between  DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW()")->count();

        $status = DELETE_STATUS;
        $role = USER_ROLE;
        $newUsers = DB::select("select count(id) as countUser, MONTH(created_at) as month from users WHERE (created_at between  DATE_FORMAT(NOW() ,'%Y-01-01') AND NOW() ) and status !=  $status and role = $role GROUP BY YEAR(created_at), MONTH(created_at)");
        $totalUser = User::count();
        $chartData = [];
        $maxUser = 1;
        for ($i = 0; $i <= 11; $i++) {
            foreach ($newUsers as $newUser) {
                if ($newUser->month - 1 == $i) {
                    $chartData[$i] = $newUser->countUser;
                }
                if ($maxUser < $newUser->countUser) {
                    $maxUser = $newUser->countUser;
                }
            }
            if (!isset($chartData[$i])) {
                $chartData[$i] = 0;
            }
        }
        $data['maxUser'] = (int) ceil($maxUser / 100) * 100;
        $data['chartData'] = $chartData;
        $data['totalUser'] = $totalUser;

        return view('admin.dashboard', $data);
    }
}
