<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


require base_path('routes/api/admin.php');
require base_path('routes/api/user.php');


Route::group(['middleware' => ['language'], 'namespace' => 'Api'], function () {

    Route::group(['middleware' => ['auth:api', 'app.user']], function () {
        Route::group(['middleware' => 'verified.user'], function () {
            Route::get('get-user-profile', 'ProfileController@getUserProfile')->name('api.getUserProfile');
            Route::post('update-user-profile', 'ProfileController@updateUserProfile')->name('api.updateUserProfile');
            Route::post('update-password', 'ProfileController@updatePassword')->name('api.updatePassword');

            Route::get('language-list', 'ProfileController@languageList')->name('api.languageList');
            Route::post('set-language', 'ProfileController@setLanguage')->name('api.setLanguage');
        });

    });
});
