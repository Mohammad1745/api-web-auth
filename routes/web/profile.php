<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Web'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('password-change', 'ProfileController@passwordChange')->name('passwordChange');
        Route::post('password-change-process', 'ProfileController@passwordChangeProcess')->name('passwordChangeProcess');
    });
});
