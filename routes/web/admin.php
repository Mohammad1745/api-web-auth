<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth', 'namespace' => 'Web\Admin', 'prefix' => 'admin'], function () {
    Route::group(['middleware' => 'admin'], function () {
        Route::get('dashboard', 'DashboardController@dashboard')->name('admin.dashboard');

        Route::get('settings', 'SettingsController@settings')->name('admin.settings');
        Route::post('settings-save-process', 'SettingsController@settingsSaveProcess')->name('admin.settingsSaveProcess');

        Route::get('user-list', 'UserController@index')->name('admin.userList');
        Route::get('user-save', 'UserController@userAdd')->name('admin.userAdd');
        Route::post('user-save-process', 'UserController@userAddProcess')->name('admin.userAddProcess');
        Route::get('user-view/{id}', 'UserController@userView')->name('admin.userView');
        Route::get('user-edit/{id}', 'UserController@userEdit')->name('admin.userEdit');
        Route::get('user-delete/{id}', 'UserController@userDelete')->name('admin.userDelete');
        Route::get('get-user-list', 'UserController@getUserList')->name('admin.getUserList');
    });
});
