<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth', 'namespace' => 'Web\User', 'prefix' => 'user'], function () {
    Route::group(['middleware' => 'app.user'], function () {
        Route::get('dashboard', 'DashboardController@dashboard')->name('user.dashboard');
    });
});
